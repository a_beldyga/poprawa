# Create your views here.
from django.shortcuts import render_to_response, redirect
from models import *
from form import *
from django.core.context_processors import csrf
from django.utils import timezone
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login
from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required

def blog(request):
    newsy= Dane.objects.all().order_by('-pk')
    users = User.objects.all()
    return render(request, 'base.html', {'newsy': newsy, 'users': users})


def add(request):

    if request.method == 'GET':
            c = {}
            c.update(csrf(request))
            return render_to_response('add.html', c)
    if request.method == 'POST':
            form = Form(request.POST) # A form bound to the POST data
            user = form.data['user']
            title = form.data['title']
            content = form.data['text']

            r = Dane(user=user, title=title, content=content,  published= timezone.now())
            r.save()
            return redirect(reverse('blog'))


    else:
        return redirect(reverse('blog'))

def user_login(request):

    context = RequestContext(request)

    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user is not None:

            if user.is_active:
                login(request, user)

              #  return HttpResponseRedirect('blog')
                return redirect(reverse('blog'))
            else:
               return HttpResponse("konto nieaktywne")
        else:

            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")

    else:
        c = {}
        c.update(csrf(request))
        return render_to_response('login.html', c)


# Use the login_required() decorator to ensure only those logged in can access the view.
@login_required
def user_logout(request):
    # Since we know the user is logged in, we can now just log them out.
    logout(request)

    # Take the user back to the homepage.
    return redirect(reverse('blog'))

def mojWpis(request, user="p6"):

  #  user=User.objects.get(id=request.user.id)
    wpisy=Dane.objects.order_by('-pk')
    wpisyUsera=[]
    for wpis in wpisy:
        if wpis.user==user:
            wpisyUsera.append(wpis)

    return render_to_response('wpisyUsera.html',{'wpisy':wpisyUsera, 'user':user})